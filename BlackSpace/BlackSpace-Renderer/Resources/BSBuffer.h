#pragma once
/*Copyright(C) Raid Studios, All Rights Reserved.
* USAGE: Helper for the usage(s) of buffers.
* NOTE: might to have to extend a bit with multithreading for this class to work with it....
*/
#include "../EngineCore/AllocatorManager.h"
#include "../EngineCore/BlackSpaceRenderCore.h"
//D3D11 INC
#include <d3d11_4.h>
#include <dxgi1_5.h>
#include <wrl.h>
//D3D12 INC
#include <d3d12.h>
#include <dxgi1_5.h>
#include "GpuResource.h"
using namespace BlackSpace;

namespace BlackSpace
{
	enum class GRAPHICS_API
	{
		API_D3D12 = 0x0,
		API_D3D11 = 0x1,
		API_VULKAN = 0x2,
	};
	class GpuBuffer : public GpuResource
	{
	public:
		template <typename A>
		using COM = Microsoft::WRL::ComPtr<A>;

		GpuBuffer(GRAPHICS_API API);

		
	protected:
		
	};

	
	
	

}
