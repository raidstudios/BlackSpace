﻿// *Copyright(C) Raid Studios, All Rights Reserved.

#pragma once
#include "../EngineCore/BlackSpaceRenderCore.h"
#include "../EngineCore/AllocatorManager.h"

namespace BlackSpace
{
	class BSTexture
	{
	public:
		BSTexture() = default;
		BSTexture(const BSTexture&) = delete;

		//Function Calls
	};
}
