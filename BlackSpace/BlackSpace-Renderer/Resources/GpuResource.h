﻿// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 

#pragma once
#include "../Helpers/BlackSpaceDirectX.h"
#include "../EngineCore/AllocatorManager.h"
#include <d3d12.h>
using namespace BlackSpace;
class GpuResource : public BSResource<GpuResource>
{
public:
	template <typename A>
	using COM = Microsoft::WRL::ComPtr<A>;
	GpuResource() : m_GpuVirtualAddress(),
		m_UsageState(D3D12_RESOURCE_STATE_COMMON),
		m_TransitioningState((D3D12_RESOURCE_STATES)-1){}
	ID3D12Resource* GetResource() { return DX12Resource.Get(); }
	auto operator->() const noexcept { return DX12Resource.operator->();  }
protected:
	COM<ID3D12Resource> DX12Resource;
	D3D12_RESOURCE_STATES m_UsageState;
	D3D12_RESOURCE_STATES m_TransitioningState;
	D3D12_GPU_VIRTUAL_ADDRESS m_GpuVirtualAddress;
};
