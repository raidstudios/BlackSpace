#pragma once
/*Copyright(C) Raid Studios, All Rights Reserved.
* USAGE:
*/
#include"../EngineCore/BSCore.h"
#include "../Managers/ThreadManager.h"
namespace BlackSpace::Worker
{
	enum class WorkerT
	{
		WT_MAINTHREAD = 0x11,
		WT_CHILDTHREAD = 0x012,
	};

	class RAIDEXPORT IWorker : public ThreadManager
	{
	public:
		IWorker() = default;
		IWorker(const IWorker&) = delete;
		IWorker(const bool& isThreaded);
		IWorker(const WorkerT& ThreadType);

		void AssignThread();
		void LeaveThread();


	protected:
		bool isThreaded;
	};
}
