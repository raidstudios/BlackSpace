
#include "ThreadManager.h"

template <typename A>
void BlackSpace::ThreadManager::AddTask(std::shared_ptr<A>& Task)
{
	lockmutex(mute);
	Queue.emplace(Task);
	++Tasks;
}

template <typename B>
std::unique_lock<std::mutex> BlackSpace::ThreadManager::lockmutex(B& mute)
{
	return std::unique_lock<std::mutex>(mute);
}

bool BlackSpace::ThreadManager::RemoveTask(ThreadTask& TasktoRemove)
{
	this->mute.unlock();
	if (Queue.empty())
	{
		return false;
	}
	TasktoRemove = std::move(Queue.front());
	Queue.pop();
	return true;
}
