﻿
#include "FenceManager.h"


FenceManager::FenceManager()
{
}

template <class A>
FenceManager::FenceManager(A FenceObject)
{
}

template <class A>
FenceManager::FenceManager(BSResource<A> FenceObject)
{
}


FenceManager::~FenceManager()
{
}

template<class A>
void FenceManager::Watch(A Object)
{
	if(A == ID3D12Fence)
	{
		
	}
	else if(A == VkFence || VkFence_T)
	{
		
	}
}


void FenceManager::StartEvent()
{
}


void FenceManager::IsFinshed()
{
}


UINT64 FenceManager::DXGetFenceVal(ID3D12Fence* FenceObject, ID3D12CommandQueue* cq, UINT64 INITVal)
{
	ThrowIfFailed(cq->Signal(FenceObject,INITVal));
	return FenceObject->GetCompletedValue();
}


