﻿// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 

#pragma once
#include "../EngineCore/BlackSpaceRenderCore.h"
#include <vulkan/vulkan.hpp>
#include "../Resources/BSWorker.h"
#include "../EngineCore/AllocatorManager.h"
#include "../Helpers/BlackSpaceDirectX.h"
using namespace BlackSpace;
class FenceManager : public BlackSpace::Worker::IWorker, public BSResource<FenceManager>
{
public:
	FenceManager();
	template<class A>
	FenceManager(A FenceObject);
	template <class A>
	FenceManager(BSResource<A> FenceObject);
	virtual ~FenceManager();
	//Watches Fence Object for a changed signal. 
	template<class A>
	void Watch(A Object);
	//Fires a Event when the gpu reaches the fence.
	void StartEvent();
	//Checks if the last frame was finshed.
	void IsFinshed();
protected:
	//Gets current fence value for D3D12.
	UINT64 DXGetFenceVal(ID3D12Fence* FenceObject,ID3D12CommandQueue* cq,UINT64 INITVal);

};

