﻿// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 

#pragma once
#include "BSD3D12Core.h"
#include "EngineCore/AllocatorManager.h"
#include "Helpers/BlackSpaceDirectX.h"
namespace BlackSpace::D3D12::Worker {
	struct D3D12Alloc
	{

		template <typename A>
		using COM = ComPtr<A>;

		COM<ID3D12CommandAllocator> CAlloc{};
		//Make SURE The CommandList Is Finshed! 
		void Kill();
	};
}