﻿// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 

#pragma once
#include "BSD3D12Core.h"
#include "EngineCore/AllocatorManager.h"
#include "Helpers/BlackSpaceDirectX.h"
#include "Resources/BSWorker.h"

namespace BlackSpace::D3D12::Worker {
	class D3D12List : public BlackSpace::Worker::IWorker
	{
	public:
		friend class D3D12Command;
		template <typename A>
		using COM = ComPtr<A>;

		COM<ID3D12GraphicsCommandList> CList;
	public:
		void CreateList(BSResource<BSD3D12>& DX12H, D3D12_COMMAND_LIST_TYPE type, ID3D12CommandAllocator& Alloc);
	};
}
