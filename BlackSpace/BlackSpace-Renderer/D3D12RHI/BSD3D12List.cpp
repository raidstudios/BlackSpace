﻿#include "BSD3D12List.h"


void BlackSpace::D3D12::Worker::D3D12List::CreateList(BSResource<BSD3D12>& DX12H, D3D12_COMMAND_LIST_TYPE type, ID3D12CommandAllocator& Alloc)
{
	ThrowIfFailed(DX12H->DX12Device->CreateCommandList(NULL, type, &Alloc, DX12H->PSO.Get(), IID_PPV_ARGS(CList.GetAddressOf())));
}
