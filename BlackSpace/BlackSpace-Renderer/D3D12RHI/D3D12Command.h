#pragma once
/*Copyright(C) Raid Studios, All Rights Reserved.
* CLASS USAGE:
* HANDLES D3D12Command Creation. RTV(S), CommandQueue,CommandList,PipelineStateObject, etc......
*/

#include "../Resources/BSWorker.h"
#include "../EngineCore/BlackSpaceRenderCore.h"
#include "../EngineCore/AllocatorManager.h"
#include "../Managers/FenceManager.h"
#include "BSD3D12Core.h"
#include <wrl.h>
#include <d3d12.h>
#include <dxgi1_5.h>
#include <Helpers/BlackSpaceDirectX.h>

#include "BSD3D12Alloc.h"
using namespace BlackSpace::D3D12;

namespace BlackSpace::D3D12::Worker
{
	
	class D3D12List : public BlackSpace::Worker::IWorker
	{
	public:
		friend class D3D12Command;
		template <typename A>
		using COM = ComPtr<A>;

		COM<ID3D12GraphicsCommandList> CList;
	public:
		void CreateList(BSResource<BSD3D12>& DX12H, D3D12_COMMAND_LIST_TYPE type, D3D12Alloc& Alloc);
	};
	
	class D3D12Command : public BlackSpace::Worker::IWorker
	{
	public:
		BSResource<BSD3D12> DX12H;
		template <typename A>
		using COM = ComPtr<A>;
		D3D12Alloc CFrames[FrameC];
	protected:
		LPCWSTR CheckCLType(D3D12_COMMAND_LIST_TYPE B)
		{
			if (B == D3D12_COMMAND_LIST_TYPE_DIRECT) { return L"GPU Direct Command List"; }
			if (B == D3D12_COMMAND_LIST_TYPE_COMPUTE) { return L"GPU Compute CommandList"; }
		}
	public:
		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvH;
		COM<ID3D12RootSignature> RSO;
		COM<ID3D12CommandQueue> CQO;
		BSResource<FenceManager> FM;
		BSResource<D3D12List> List;
	public:
		virtual bool CreateCommand(D3D12_COMMAND_LIST_TYPE CAllocType, D3D12_COMMAND_LIST_TYPE CLISTType,D3D12Alloc Alloc);
		virtual void StartFrame();
		virtual void EndFrame();
	};
	
	
}
