#pragma once
/*Copyright(C) Raid Studios, All Rights Reserved.
* CLASS USAGE:
* HANDLES BASIC D3D12 CREATION,COMMANDS,AND FENCES.
*/

#define _d3d12_h_
#include "../EngineCore/BlackSpaceRenderCore.h"
#include "../Helpers/BlackSpaceDirectX.h"
#include <EngineCore/AllocatorManager.h>
//D3D12 Includes
#include <dxgi1_5.h>
#include <wrl.h>
#include <d3d12.h>

#include "DXGI/DXGIManager.h"

using namespace BlackSpace::window_handles;
using namespace BlackSpace::monitor_info;
namespace BlackSpace::D3D12
{
	static const  UINT FrameC = 3;
}
namespace BlackSpace
{
	
	class BSD3D12
	{
	public:
		template <typename A>
		using COM = ComPtr<A>;
	protected:
		D3D_FEATURE_LEVEL FeatureLevel = D3D_FEATURE_LEVEL_12_1;
		D3D_FEATURE_LEVEL FeatureLevelMinimum = D3D_FEATURE_LEVEL_11_0;
	public:
		COM<ID3D12Device6> DX12Device{};
		COM<ID3D12PipelineState> PSO;
		COM<ID3D12DescriptorHeap> DHO;
		UINT RTVDS{};
	public:
		BSResource<DXGIManager> dxgi;
		//Fence Objects
		//NOTE: THE FENCE MANAGER WILL HANDLE FENCE EVENTS!
		UINT FrameIndex{};
		COM<ID3D12Fence1> DXFence{};
		HANDLE DXFenceEvent;
		UINT64 DXFenceVal{};

	public:
		//Debug Objects
		COM<ID3D12Debug> DXDebug{};
		COM<ID3D12DebugDevice> DeviceDebug{};

	public:
		WH WH12{};
		MI MI12{ };
		bool Initialize();
		void SetQueueBreaks();
		void Shutdown();
		void Start();
	};
}
 