
#include "BSD3D12Core.h"

using namespace BlackSpace::D3D12;
bool BlackSpace::BSD3D12::Initialize()
{
#if defined(DEBUG) || (_DEBUG)
	if (FAILED(D3D12GetDebugInterface(IID_PPV_ARGS(&DX12Device))))
	{
		//Return to the manager, that we failed to check for debug!
		return false;
	}
	DXDebug->EnableDebugLayer();
#endif
	
	//Creates D3D12 Device.
	if (FAILED(D3D12CreateDevice(nullptr, FeatureLevel, IID_PPV_ARGS(&DX12Device))))
	{
		if (FAILED(D3D12CreateDevice(nullptr, FeatureLevelMinimum, IID_PPV_ARGS(&DX12Device))))
		{
			return false;
		}
		return false;
	}
	//Sets up our Descriptor Heap.
	D3D12_DESCRIPTOR_HEAP_DESC dh;
	dh.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	dh.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	dh.NumDescriptors = D3D12::FrameC;
	ThrowIfFailed(DX12Device->CreateDescriptorHeap(&dh, IID_PPV_ARGS(&DHO)));
	ThrowIfFailed(RTVDS = DX12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV));
	
	//Set Queue Breaks.
	SetQueueBreaks();
	
	return true;
}

void BlackSpace::BSD3D12::SetQueueBreaks()
{
	COM<ID3D12InfoQueue> IFQueue{};
	DX12Device->QueryInterface(IID_PPV_ARGS(&IFQueue));
	IFQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_CORRUPTION, true);
	IFQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_ERROR, true);
#if defined(_DEBUG)
	IFQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_WARNING, true);
#endif
}

void BlackSpace::BSD3D12::Shutdown()
{
	DeviceDebug->ReportLiveDeviceObjects(D3D12_RLDO_DETAIL);
	RELEASE_COM(DX12Device);
	RELEASE_COM_IU(DXDebug);
	RELEASE_COM_DXGI(dxgi->GetSwap());
	RELEASE_COM(DXFence);
}

void BlackSpace::BSD3D12::Start()
{
	
}
