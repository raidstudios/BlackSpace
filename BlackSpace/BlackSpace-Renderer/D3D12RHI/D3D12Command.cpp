
#include "D3D12Command.h"
#include "DXGI/DXGIManager.h"
using namespace BlackSpace::D3D12;
bool BlackSpace::D3D12::Worker::D3D12Command::CreateCommand(D3D12_COMMAND_LIST_TYPE CAllocType, D3D12_COMMAND_LIST_TYPE CLISTType, D3D12Alloc Alloc)
{
	
	D3D12_COMMAND_QUEUE_DESC qd{};
	qd.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	qd.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
	qd.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	ThrowIfFailed(DX12H->DX12Device->CreateCommandQueue(&qd, IID_PPV_ARGS(&CQO)));
	//Set RTV Handle
	D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle(DX12H->DHO->GetCPUDescriptorHandleForHeapStart());
	rtvH == rtvHandle;
	for (int a{ 0 }; a < FrameC; a++) {
		COM<ID3D12Resource> BackBuffer;
		auto rtvDescriptorSize = DX12H->DX12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
		ThrowIfFailed(DX12H->dxgi->GetSwap()->GetBuffer(a, IID_PPV_ARGS((&BackBuffer))));
		DX12H->DX12Device->CreateRenderTargetView(BackBuffer.Get(), nullptr, rtvHandle);
		rtvH.Offset(1, rtvDescriptorSize);
	}
	ThrowIfFailed(DX12H->DX12Device->CreateCommandAllocator(CAllocType, IID_PPV_ARGS(&Alloc.CAlloc)));
	List->CreateList(DX12H, CLISTType, CFrames[0]);
	DX12H->dxgi->CreateSwap(DX12H->WH12.WindowHandle,DX12H->WH12.WidthCallback,DX12H->WH12.HeightCallback,DX12H->WH12.isWindowed,CQO.Get(),FrameC);
	return true;
}

void BlackSpace::D3D12::Worker::D3D12Command::StartFrame()
{
	D3D12Alloc FrameIn{ CFrames[DX12H->DXFenceVal] };
	FM->Watch(DX12H->DXFence);
	ThrowIfFailed(List->CList->Reset(FrameIn.CAlloc.Get(), DX12H->PSO.Get()));
}

void BlackSpace::D3D12::Worker::D3D12Command::EndFrame()
{
	ThrowIfFailed(List->CList->Close());
	ID3D12CommandList* const lists[]{ List->CList.Get() };
	this->CQO->ExecuteCommandLists(_countof(lists), &lists[0]);
	DX12H->DXFenceVal = (DX12H->DXFenceVal + 1) % FrameC;
}
