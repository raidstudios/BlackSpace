// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 
#pragma once
#include "../EngineCore/BlackSpaceRenderCore.h"
#include "../EngineCore/AllocatorManager.h"
#include <fbxsdk.h>
#include <mutex>


namespace BlackSpace::Importer {
	struct FBXScene
	{
		
	};
	struct SceneData
	{
		
	};
	struct mesh
	{
		
	};
	struct geo_imp_settings
	{
		
	};
	class FBXContext {
	public:
		//fbx sdk is SINGLE THREADED
		std::mutex fbx_mute;
		FBXContext(std::string file,FBXScene* scene, SceneData* sceneData);
		virtual ~FBXContext();
		//FBX Contexts
		bool CreateFBXContext();
		bool LoadFBX();
		void InitFBXFile(std::string file);
		//Utils
		const float SceneScale() { return BaseSceneScale; }
	protected:
		FBXScene* Scene{};
		SceneData Scene_Data{};
		FbxScene* FBXS;
		FbxManager* FBXM{ nullptr };
		float BaseSceneScale = 1.0f;
	};
}