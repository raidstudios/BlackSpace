#include "FbxImporter.h"
using namespace BlackSpace::Importer;
FBXContext::FBXContext(std::string file, FBXScene* scene, SceneData* sceneData)
{
	//First Lets Check if the Params are valid.
	RAID_ASSERT(scene);
	RAID_ASSERT(sceneData);
	if (CreateFBXContext() == true) {
		InitFBXFile(file);
	}
	//heck if the Params are valid after the call.
	RAID_ASSERT(scene);
	RAID_ASSERT(sceneData);
}

FBXContext::~FBXContext()
{
	//Delete All Resources.
	FBXS->Destroy();
	FBXM->Destroy();
	delete& Scene;
	delete& Scene_Data;
	delete& FBXS;
	delete& FBXM;
}

bool BlackSpace::Importer::FBXContext::CreateFBXContext()
{
	FBXM = FbxManager::Create();
	RAID_ASSERT(FBXM);
	if (!FBXM) { return false; }
	FbxIOSettings* io = FbxIOSettings::Create(FBXM, IOSROOT);
	RAID_ASSERT(io);
	return true;
}

bool FBXContext::LoadFBX()
{
	return false;
}

void FBXContext::InitFBXFile(std::string file)
{
	FBXS = FbxScene::Create(FBXM, "Import Scene");
	RAID_ASSERT(FBXS);
	FbxImporter* importer = FbxImporter::Create(FBXM, "File Importer");
	RAID_ASSERT(importer);
	if(!(importer)&&(importer->Initialize(file.c_str(),-1,FBXM->GetIOSettings())&& importer->Import(FBXS)))
	{
		return;
	}
	importer->Destroy();
	BaseSceneScale = static_cast<float>(FBXS->GetGlobalSettings().GetSystemUnit().GetScaleFactor());
}
