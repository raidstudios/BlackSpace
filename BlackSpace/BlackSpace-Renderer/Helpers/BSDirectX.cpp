
#include <wrl/client.h>

#include "BlackSpaceDirectX.h"

void SetDebugN(const ComPtr<ID3D12Object>& a, LPCWSTR Name)
{
	a->SetName(Name);
}

void ReleaseComD3D12(const ComPtr<ID3D12Object>& Object)
{
	Object->Release();
}

void ReleaseComIU(const ComPtr<IUnknown>& Object)
{
	Object->Release();
}

void SetDebugPageable(const ComPtr<ID3D12Pageable>& b, LPCWSTR Name)
{
	b->SetName(Name);
}

void ReleaseComDXGI(const ComPtr<IDXGIObject>& Object)
{
	Object->Release();
}
