#pragma once
#pragma once
/*Copyright(C) Raid Studios, All Rights Reserved.
*/

//Basic Includes
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstdio>
#include <cstdint>
#include <thread>
#include <ctime>
#include <map>
#include <list>
#include <algorithm>
#include <memory>
#include <string>
#include <vector>
#include<functional>
#include <thread>
#include <list>
#include <chrono>
#include <deque>
#include <queue>
#include <combaseapi.h>
#include <cassert>
//Engine Defines
// The Engine now exports as a DLL we should remove all Export/Import Calls.
//#define RAIDEXPORT __declspec(dllexport)
//#define RAIDIMPORT __declspec(dllimport)
#define RAID_ASSERT assert
template <typename A>
using UP = std::unique_ptr<A>;
namespace BlackSpace::window_handles
{
	class WH
	{
	public:
		HWND WindowHandle;
		bool isWindowed;
		int WidthCallback;
		int HeightCallback;
		bool IsResizeing;
	};
}
namespace BlackSpace::monitor_info
{
	class MI
	{
	public:
		UINT MonitorRefresh();
		UINT MonitorWidth();
		UINT MonitorHeight();
		bool SupportsHDR();
		auto AspectRatio();
	};
}
