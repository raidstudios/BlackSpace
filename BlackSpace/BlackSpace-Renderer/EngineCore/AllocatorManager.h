#pragma once
/*Copyright(C) Raid Studios, All Rights Reserved.*/
#include "BSCore.h"
#include <wrl.h>
#include <string_view>

namespace BlackSpace
{
	template<class A>
	class RAIDEXPORT BSResource : public std::_Ptr_base<A>
	{
	public:
		using _Mybase = std::_Ptr_base<A>;

		using COM = Microsoft::WRL::ComPtr<A>;
		BSResource(A Resource);
		BSResource() = default;
		virtual ~BSResource();

		A* GetResource();
		A& GetAddress();
		void ResetResource();
		void DeleteResource();
		A SwapResource(const BSResource& Resource);
		A* operator->() const noexcept;
	};

	template <class A>
	A* BSResource<A>::GetResource()
	{
		return *_Mybase;
	}
	template<class A>
	 A& BSResource<A>::GetAddress()
	{
		 return &A;
	}

	template<class A>
	void BSResource<A>::ResetResource(){ZeroMemory(this, sizeof(this));}
	 
}
