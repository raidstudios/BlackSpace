#pragma once
/*Copyright(C) Raid Studios, All Rights Reserved.
* USAGE: Minimized Version of Core Lib
*/

//Basic Includes
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstdio>
#include <cstdint>
#include <thread>
#include <ctime>
#include <map>
#include <list>
#include <algorithm>
#include <memory>
#include <string>
#include <vector>
#include<functional>
#include <thread>
#include <list>
#include <chrono>
#include <deque>
#include <queue>
//Engine Defines

#define RAIDEXPORT __declspec(dllexport)
#define RAIDIMPORT __declspec(dllimport)
