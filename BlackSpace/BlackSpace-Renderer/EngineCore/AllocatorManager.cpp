#include "AllocatorManager.h"

template <class A>
BlackSpace::BSResource<A>::BSResource(A Resource)
{
	
}

template <class A>
BlackSpace::BSResource<A>::~BSResource()
{
	this->DeleteResource();
}

template<class A>
void BlackSpace::BSResource<A>::DeleteResource()
{
	delete this->GetResource();
}

template <class A>
A BlackSpace::BSResource<A>::SwapResource(const BSResource& Resource)
{
	auto determine = A;
	std::move_if_noexcept(A, Resource);
}

template<class A>
A* BlackSpace::BSResource<A>::operator->() const noexcept
{
	return *_Mybase::_Ptr;
}
