#pragma once
#include "../EngineCore/BlackSpaceRenderCore.h"
#include "Helpers/BlackSpaceDirectX.h"
#include <dxgi1_6.h>
class DXGIManager
{
public:
	DXGIManager();
	virtual ~DXGIManager();
	void CreateSwap(HWND WindowH, int Width, int Height, bool isWin, ID3D12CommandQueue* Q,UINT FrameC);
	IDXGIFactory* GetFactory() const { return Factory.Get(); }
	IDXGISwapChain4* GetSwap() const { return Swap.Get(); }
protected:
	ComPtr<IDXGIFactory> Factory;
	ComPtr<IDXGISwapChain4> Swap;
};

