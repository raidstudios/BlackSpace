#include "DXGIManager.h"

DXGIManager::DXGIManager()
{
    ThrowIfFailed(CreateDXGIFactory2(0, IID_PPV_ARGS(&Factory)));
}

DXGIManager::~DXGIManager()
{
}

void DXGIManager::CreateSwap(HWND WindowH, int Width, int Height, bool isWin, ID3D12CommandQueue* Q, UINT FrameC)
{

    // Describe and create the swap chain.
    DXGI_SWAP_CHAIN_DESC swapChainDesc = {};
    swapChainDesc.BufferCount = FrameC;
    swapChainDesc.BufferDesc.Width = Width;
    swapChainDesc.BufferDesc.Height = Height;
    swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
    swapChainDesc.OutputWindow = WindowH;
    swapChainDesc.SampleDesc.Count = 1;
    swapChainDesc.Windowed = isWin;

    ComPtr<IDXGISwapChain> swapChain;
    ThrowIfFailed(Factory->CreateSwapChain(
       Q,        // Swap chain needs the queue so that it can force a flush on it.
        &swapChainDesc,
        &swapChain
    ));

    ThrowIfFailed(swapChain.As(&Swap));
}
