// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 
#pragma once
#include "BSD3D11Core.h"

class D3D11CList {
public:
	template<typename A>
	using COM = Microsoft::WRL::ComPtr<A>;

	void CreateCList();
	void FinshRecordingCommands();

	COM<ID3D11CommandList> CList;
	COM<ID3D11DeviceContext4> Context;
};