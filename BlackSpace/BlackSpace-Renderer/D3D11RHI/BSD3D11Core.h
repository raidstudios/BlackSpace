#pragma once
// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 
#include<d3d11_4.h>
#include <dxgi1_6.h>
#include "../Helpers/BlackSpaceDirectX.h"
#include "../EngineCore/BlackSpaceRenderCore.h"
namespace BlackSpace {

	class BSD3D11 {
	public:
		template<typename A>
		using COM = Microsoft::WRL::ComPtr<A>;
	protected:
		//Window Objects
		BlackSpace::window_handles::WH wh;
	public:
		//D3D11 Objects
		COM<ID3D11Device5> Device = nullptr;
		COM<ID3D11DeviceContext4> CoreContext = nullptr;
		COM<ID3D11Debug> Debug = nullptr;
		COM<ID3D11RenderTargetView> RTV = nullptr;
		COM<ID3D11DepthStencilView> DTV = nullptr;
		COM<ID3D11Texture2D1> BBTexture = nullptr;
		COM<ID3D11Texture2D1> DBTexture = nullptr;
		COM<ID3D11RasterizerState> Rasterize = nullptr;
		COM<ID3D11ShaderResourceView1> BaseSRV = nullptr;
		COM<ID3D11Buffer> BackBuffer = nullptr;
		D3D_FEATURE_LEVEL FEAT = D3D_FEATURE_LEVEL_11_1;
		UINT DFLAGS = D3D11_CREATE_DEVICE_FLAG::D3D11_CREATE_DEVICE_BGRA_SUPPORT;
		UINT SUPPORTRES{};
		float color[4] = { 0.1f,0.1f,0.1f,1.0f};
		//DXGI Objects
		COM<IDXGIFactory2> Factory = nullptr;
		COM<IDXGIAdapter> GPU = nullptr;
		COM<IDXGISwapChain> Swap = nullptr;
		//FenceObjects
		COM<ID3D11Fence> Fence;
		HANDLE FHandle;
		UINT64 FenceV;
		//Methods
		void CreateContexts();
		void CreateSwap();
		void SetupFence();
		void Present();
		std::vector<IDXGIAdapter*> GPUS = enumerate_adapters();
	protected:
		bool IsResize() { return wh.IsResizeing; }
		void SetupBaseAssets();
		std::vector <IDXGIAdapter*> enumerate_adapters()
		{
			IDXGIAdapter* pAdapter;
			std::vector <IDXGIAdapter*> vAdapters;
			IDXGIFactory* pFactory = nullptr;
			// Create a DXGIFactory object.
			//NOTE: USE IID_PPV_ARGS EVEN WITH DIRECTX11
			if (FAILED(CreateDXGIFactory(IID_PPV_ARGS(&Factory))))
			{
				return vAdapters;
			}
			for (UINT i = 0;
				pFactory->EnumAdapters(i, &pAdapter) != DXGI_ERROR_NOT_FOUND;
				++i)
			{
				vAdapters.push_back(pAdapter);
			}
			if (pFactory)
			{
				pFactory->Release();
			}

			return vAdapters;

		}

	};
}