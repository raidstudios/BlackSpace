
#include "BSD3D11Core.h"

void BlackSpace::BSD3D11::CreateContexts()
{
	COM<ID3D11Device> D;
	COM<ID3D11DeviceContext> C;
	ThrowIfFailed(CreateDXGIFactory(IID_PPV_ARGS(&Factory)));
	ThrowIfFailed(D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, DFLAGS, &FEAT, 1, 
		D3D11_SDK_VERSION, &D, &FEAT,&C));
	ThrowIfFailed(D.As(&Device));
	ThrowIfFailed(C.As(&CoreContext));
	//Checks if the Device Supports our Swap Format
	ThrowIfFailed(Device->CheckFormatSupport(DXGI_FORMAT_B8G8R8A8_UNORM, &SUPPORTRES));
}
//SwapChain Should be Handled by our DXGI Manager. Once its finshed, we should replace this function
void BlackSpace::BSD3D11::CreateSwap()
{
    COM<IDXGISwapChain1> tempswap;

    DXGI_SWAP_CHAIN_DESC1 SWD{};

    SWD.Width = wh.WidthCallback;
    SWD.Height = wh.HeightCallback;
    SWD.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
    SWD.SampleDesc.Count = 1;
    SWD.SampleDesc.Quality = 0;
    SWD.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    SWD.BufferCount = 2;
    SWD.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
    SWD.Flags = {};
    DXGI_SWAP_CHAIN_FULLSCREEN_DESC FSDESC = {};
    FSDESC.Windowed = wh.isWindowed;
    if (FAILED(Factory->CreateSwapChainForHwnd(Device.Get(), wh.WindowHandle, &SWD, &FSDESC, nullptr, tempswap.GetAddressOf()))) {return; }
}

void BlackSpace::BSD3D11::SetupFence()
{
    auto Fencef = D3D11_FENCE_FLAG::D3D11_FENCE_FLAG_NONE;
    ThrowIfFailed(Device->CreateFence(FenceV, Fencef, IID_PPV_ARGS(&Fence)));
}

void BlackSpace::BSD3D11::SetupBaseAssets()
{
    //Get BackBuffer From SwapChain.
    ThrowIfFailed(Swap->GetBuffer(1, IID_PPV_ARGS(BBTexture.GetAddressOf())));
    //Setup RTV,SRV,BackBuffer Textures.
    D3D11_TEXTURE2D_DESC1 t{};
    D3D11_RENDER_TARGET_VIEW_DESC r{};
    D3D11_SHADER_RESOURCE_VIEW_DESC1 shaderResourceViewDesc{};
    D3D11_DEPTH_STENCIL_VIEW_DESC d{};
    //rtv desc
    r.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    r.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
    r.Texture2D.MipSlice = NULL;
    //TextureDesc
    t.Width = wh.WidthCallback;
    t.Height = wh.HeightCallback;
    t.Format = r.Format;
    t.SampleDesc.Count = 1;
    t.MipLevels = 1;
    t.ArraySize = 1;
    t.Usage = D3D11_USAGE_DEFAULT;
    t.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
    t.CPUAccessFlags = 0;
    t.MiscFlags = 0;
    // Setup the description of the shader resource view.
    shaderResourceViewDesc.Format = t.Format;
    shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
    shaderResourceViewDesc.Texture2D.MipLevels = 1;
    //Setup Depth Stencil View
    d.Format = DXGI_FORMAT_R24G8_TYPELESS;
    d.Texture2D.MipSlice = NULL;
    d.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    ThrowIfFailed(Device->CreateTexture2D1(&t, NULL, BBTexture.GetAddressOf()));
    ThrowIfFailed(Device->CreateRenderTargetView(BackBuffer.Get(), &r, RTV.GetAddressOf()));
    ThrowIfFailed(Device->CreateShaderResourceView1(BBTexture.Get(), &shaderResourceViewDesc, BaseSRV.GetAddressOf()));
    ThrowIfFailed(Device->CreateDepthStencilView(DBTexture.Get(), &d, DTV.GetAddressOf()));
}

void BlackSpace::BSD3D11::Present()
{
    auto rtvs = { RTV.Get() };
    CoreContext->ClearRenderTargetView(RTV.Get(),color);
    CoreContext->OMSetRenderTargets(1, RTV.GetAddressOf(),DTV.Get());
}
