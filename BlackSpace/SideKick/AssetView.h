#pragma once

#include <QWidget>
#include "ui_AssetView.h"

class AssetView : public QWidget
{
	Q_OBJECT

public:
	AssetView(QWidget *parent = Q_NULLPTR);
	~AssetView();
public slots:
	/**
	 * \brief Shows Only Models. Should Be Triggered by a QAction.
	 */
	void ShowOnlyModels();
	/**
	 * \brief Shows Only Textures. Should Be Triggered by a QAction.
	 */
	void ShowOnlyTextures();
	/**
	 * \brief Shows Only Shaders. Should Be Triggered by a QAction.
	 */
	void ShowOnlyShaders();
	/**
	 * \brief Shows Built Prefabs Only. Should Be Triggered by a QAction.
	 */
	void ShowOnlyPrefabs();
	/**
	 * \brief Shows Only Built Maps. Should Be Triggered by a QAction.
	 */
	void ShowOnlyMaps();

private:
	Ui::AssetView ui;
};
