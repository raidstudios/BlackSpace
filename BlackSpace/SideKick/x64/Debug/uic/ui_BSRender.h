/********************************************************************************
** Form generated from reading UI file 'BSRender.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BSRENDER_H
#define UI_BSRENDER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BSRender
{
public:

    void setupUi(QWidget *BSRender)
    {
        if (BSRender->objectName().isEmpty())
            BSRender->setObjectName(QString::fromUtf8("BSRender"));
        BSRender->resize(593, 345);

        retranslateUi(BSRender);

        QMetaObject::connectSlotsByName(BSRender);
    } // setupUi

    void retranslateUi(QWidget *BSRender)
    {
        BSRender->setWindowTitle(QCoreApplication::translate("BSRender", "BSRender", nullptr));
    } // retranslateUi

};

namespace Ui {
    class BSRender: public Ui_BSRender {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BSRENDER_H
