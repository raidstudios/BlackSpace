/********************************************************************************
** Form generated from reading UI file 'SideKick.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIDEKICK_H
#define UI_SIDEKICK_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "BSRender.h"

QT_BEGIN_NAMESPACE

class Ui_SideKickClass
{
public:
    QAction *actionMapEd;
    QWidget *centralWidget;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    BSRender *widget;
    QListView *listView;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *SideKickClass)
    {
        if (SideKickClass->objectName().isEmpty())
            SideKickClass->setObjectName(QString::fromUtf8("SideKickClass"));
        SideKickClass->resize(1459, 893);
        actionMapEd = new QAction(SideKickClass);
        actionMapEd->setObjectName(QString::fromUtf8("actionMapEd"));
        centralWidget = new QWidget(SideKickClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(60, 10, 771, 471));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        listWidget = new AssetList(verticalLayoutWidget);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));

        verticalLayout->addWidget(listWidget);

        verticalLayoutWidget_2 = new QWidget(centralWidget);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(890, 10, 361, 251));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        widget = new BSRender(verticalLayoutWidget_2);
        widget->setObjectName(QString::fromUtf8("widget"));

        verticalLayout_2->addWidget(widget);

        listView = new QListView(centralWidget);
        listView->setObjectName(QString::fromUtf8("listView"));
        listView->setGeometry(QRect(895, 281, 361, 391));
        SideKickClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(SideKickClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1459, 21));
        SideKickClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(SideKickClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        SideKickClass->addToolBar(Qt::LeftToolBarArea, mainToolBar);
        statusBar = new QStatusBar(SideKickClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        SideKickClass->setStatusBar(statusBar);

        retranslateUi(SideKickClass);

        QMetaObject::connectSlotsByName(SideKickClass);
    } // setupUi

    void retranslateUi(QMainWindow *SideKickClass)
    {
        SideKickClass->setWindowTitle(QCoreApplication::translate("SideKickClass", "SideKick", nullptr));
        actionMapEd->setText(QCoreApplication::translate("SideKickClass", "Map Editor", nullptr));
#if QT_CONFIG(tooltip)
        actionMapEd->setToolTip(QCoreApplication::translate("SideKickClass", "Open The Engine Map Editor.", nullptr));
#endif // QT_CONFIG(tooltip)
    } // retranslateUi

};

namespace Ui {
    class SideKickClass: public Ui_SideKickClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SIDEKICK_H
