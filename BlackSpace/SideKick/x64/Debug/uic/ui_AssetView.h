/********************************************************************************
** Form generated from reading UI file 'AssetView.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ASSETVIEW_H
#define UI_ASSETVIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AssetView
{
public:

    void setupUi(QWidget *AssetView)
    {
        if (AssetView->objectName().isEmpty())
            AssetView->setObjectName(QString::fromUtf8("AssetView"));
        AssetView->resize(400, 300);

        retranslateUi(AssetView);

        QMetaObject::connectSlotsByName(AssetView);
    } // setupUi

    void retranslateUi(QWidget *AssetView)
    {
        AssetView->setWindowTitle(QCoreApplication::translate("AssetView", "AssetView", nullptr));
    } // retranslateUi

};

namespace Ui {
    class AssetView: public Ui_AssetView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ASSETVIEW_H
