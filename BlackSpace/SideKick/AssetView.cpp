#include "AssetView.h"

AssetView::AssetView(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	
}

AssetView::~AssetView()
{
}

void AssetView::ShowOnlyModels()
{
}

void AssetView::ShowOnlyTextures()
{
}

void AssetView::ShowOnlyShaders()
{
}

void AssetView::ShowOnlyPrefabs()
{
}

void AssetView::ShowOnlyMaps()
{
}
