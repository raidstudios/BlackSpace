#include "BSRender.h"

BSRender::BSRender(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
}

BSRender::~BSRender()
{
}

void BSRender::SetupD3D11()
{
	w->HeightCallback = BSRender::sizeHint().height();
	w->WidthCallback = BSRender::sizeHint().width();
	w->WindowHandle = reinterpret_cast<HWND>(BSRender::winId());
	w->isWindowed = false;
	D11->CreateContexts();
	D11->CreateSwap();
}

void BSRender::SetupBaseAssets()
{
}

void BSRender::Render()
{
}
