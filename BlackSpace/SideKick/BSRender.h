#pragma once

#include <QWidget>
#include "ui_BSRender.h"
#include <BlackSpace-Renderer/D3D11RHI/BSD3D11Core.h>
using namespace BlackSpace;
using namespace BlackSpace::window_handles;
class BSRender : public QWidget
{
	Q_OBJECT

public:
	BSRender(QWidget *parent = Q_NULLPTR);
	~BSRender();

	void SetupD3D11();
	void SetupBaseAssets();
	void Render();
private:
	Ui::BSRender ui;
	UP<BSD3D11> D11;
	UP<WH> w;
};
