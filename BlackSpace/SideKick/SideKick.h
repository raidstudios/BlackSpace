#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_SideKick.h"

class SideKick : public QMainWindow
{
    Q_OBJECT

public:
    SideKick(QWidget *parent = Q_NULLPTR);
    void ConnectActions();

private:
    Ui::SideKickClass ui;
};
