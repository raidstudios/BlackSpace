// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 
#pragma once
#include <BlackSpace/BlackSpaceCore.h>
#define RRES_IMPLEMENTATION
#include <rres.h>

class BSRRESCore {
public:
    int Resorces{ 0 };
	void CreateRRESFile(std::string name) {
		RRESFile = fopen(name.c_str(), "wr");
		assert(RRESFile);
        fwrite(&header, sizeof(header), 1, RRESFile);
	}  
    void PackFile(std::string FileData,char filetype[4]) {
        rresResourceChunkInfo CI = {};
        rresResourceChunkData CD = {};
        unsigned int raw = strlen(FileData.c_str());
        unsigned char* DataBuffer = NULL;
        CI.type[0] = filetype[0];
        CI.type[1] = filetype[1];
        CI.type[2] = filetype[2];
        CI.type[3] = filetype[3];
        auto string = FileData.c_str();
        CI.id = rresComputeCRC32((unsigned char*)string, strlen(FileData.c_str()));
        CI.compType = RRES_COMP_LZ4;
        CI.cipherType = RRES_CIPHER_AES;
        CI.flags = NULL;
        CI.baseSize = 5 * sizeof(unsigned int) + raw;
        CI.packedSize = CI.baseSize;
        CI.nextOffset = 0;
        CI.reserved = 0;
        CD.propCount = 4;
        CD.props = (unsigned int*)RRES_CALLOC(CD.propCount, sizeof(unsigned int));
        CD.props[0] = raw;
        CD.props[1] = RRES_TEXT_ENCODING_UNDEFINED;
        CD.props[2] = RRES_CODE_LANG_UNDEFINED;
        CD.props[3] = 0x0409;
        CD.raw = (void*)FileData.c_str();
        DataBuffer = LoadDataBuffer(CD, raw);
        CI.crc32 = rresComputeCRC32(DataBuffer, CI.packedSize);
        //Write resource chunk into file
        fwrite(&CI, sizeof(rresResourceChunkInfo), 1, RRESFile);
        fwrite(DataBuffer, CI.packedSize, 1, RRESFile);

        memset(&CI,0,sizeof(rresResourceChunkInfo));
        RRES_FREE(CD.props);
        UnloadDataBuffer(DataBuffer);
    }
protected:
	FILE* RRESFile;
    rresFileHeader header = {
        header.id[0] = 'r',           // File identifier: rres
        header.id[1] = 'r',           // File identifier: rres
        header.id[2] = 'e',           // File identifier: rres
        header.id[3] = 's',           // File identifier: rres
        header.version = 100,         // File version: 100 for version 1.0
        header.chunkCount = Resorces,        // Number of resource chunks in the file (MAX: 65535)
        header.cdOffset = 0,          // Central Directory offset in file (0 if not available)
        header.reserved = 0           // <reserved>
    };
    // Load a continuous data buffer from rresResourceChunkData struct
    static unsigned char* LoadDataBuffer(rresResourceChunkData data, unsigned int rawSize)
    {
        unsigned char* buffer = (unsigned char*)RRES_CALLOC((data.propCount + 1) * sizeof(unsigned int) + rawSize, 1);

        memcpy(buffer, &data.propCount, sizeof(unsigned int));
        for (int i = 0; i < data.propCount; i++) memcpy(buffer + (i + 1) * sizeof(unsigned int), &data.props[i], sizeof(unsigned int));
        memcpy(buffer + (data.propCount + 1) * sizeof(unsigned int), data.raw, rawSize);

        return buffer;
    }

    // Unload data buffer
    static void UnloadDataBuffer(unsigned char* buffer)
    {
        RRES_FREE(buffer);
    }
};