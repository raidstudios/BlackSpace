#pragma once
// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 
#include "RCore.h"
#include "DirectXMath/DirectXMath.h"
using namespace DirectX;
class Ray
{
public:

	Ray(XMVECTOR Orgin);

	/// <summary>
	/// Creates A Ray that Takes The Inital Point of the parent Ray.
	/// </summary>
	/// <param name="InheritingRay">
	/// Ray to inherit From.
	/// </param>
	Ray(const Ray& InheritingRay);
	/// <summary>
	/// Checks if the Ray Has Hit A Surface.
	/// </summary>
	/// <returns> Returns True if Hit; Returns False if not Hit.</returns>
	bool Hit();
	/// <summary>
	/// Initalizes A Ray With a New Normal Position.
	/// </summary>
	Ray(XMVECTOR Orgin, XMVECTOR Normal);
	 /// <summary>
	 /// Checks if a World Point Matches A Vectors Start or End.
	 /// </summary>
	bool PointIsSame(XMVECTOR Point);
protected:
	XMVECTOR Org;
	XMVECTOR End;
	XMVECTOR Normal{ 0.0f,0.0f,1.0f };
	XMVECTOR Direction;
};