#pragma once
 /*Copyright(C) Raid Studios, All Rights Reserved.
*/

//Basic Includes
#define _CRT_SECURE_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstdio>
#include <cstdint>
#include <thread>   
#include <ctime>
#include <map>
#include <list>
#include <algorithm>
#include <memory>
#include <string>
#include <vector>
#include <cassert>

#define RAID_ASSERT assert