workspace "RaidStudios"
configuations {"Debug", "Release"}

project "BlackSpace"
kind "Libary"
language "C++"
targetdir "bin/"

files { "**.h", "**.c", "**.cpp", "**.bsefg"}


filter "configurations:Debug"
defines { "DEBUG" }
symbols "On"

filter "configurations:Release"
defines { "NDEBUG" }
optimize "On"

project "BlackSpaceRenderer"
kind ""