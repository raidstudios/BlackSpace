#include "Vector3.h"



BlackSpace::Math::Vector3::Vector3(float X, float Y, float Z)
{
    X = A;
    Y = B;
    Z = C;
}

bool BlackSpace::Math::Vector3::IsEqual(const Vector3& Vec)
{
    if (!this->A == Vec.A && this->B == Vec.B && this->C == Vec.C) {
        return false;
    }
    else {
        return true;
    }
}

auto BlackSpace::Math::Vector3::operator *(const Vector3& Vec)
{
    this->A *= Vec.A;
    this->B *= Vec.B;
    this->C *= Vec.B;
    return this;
}

auto BlackSpace::Math::Vector3::operator /(const Vector3& Vec)
{
    this->A /= Vec.A;
    this->B /= Vec.B;
    this->C /= Vec.B;
    return this;
}

auto BlackSpace::Math::Vector3::operator +(const Vector3& Vec)
{
    this->A += Vec.A;
    this->B += Vec.B;
    this->C += Vec.B;
    return this;
}

auto BlackSpace::Math::Vector3::operator -(const Vector3& Vec)
{
    this->A -= Vec.A;
    this->B -= Vec.B;
    this->C -= Vec.B;
    return this;
}

float BlackSpace::Math::Vector3::Magnitude(const Vector3& Vec)
{
    return sqrtf((float)Vec.A * Vec.A + Vec.B * Vec.B + Vec.C * Vec.C);
}

auto BlackSpace::Math::Vector3::Normalize(const Vector3& Vec)
{
    return (Vec.A,Vec.B,Vec.C / Magnitude(Vec));
}

void BlackSpace::Math::Vector3::operator==(float x) const
{
    
}
