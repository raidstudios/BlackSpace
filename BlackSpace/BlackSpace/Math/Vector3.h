#pragma once
/*Copyright(C) Raid Studios, All Rights Reserved.
* USAGE: 
*/
#include "../Core/BlackSpaceCore.h"
#include "../Core/BlackSpaceIOLog.h"

namespace BlackSpace::Math {
	struct Vector3 {
	public:
		float A, B, C;
		Vector3() =  default;
		Vector3(float X, float Y, float Z);

		bool IsEqual(const Vector3& Vec);
		auto operator *(const Vector3& Vec);
		auto operator /(const Vector3& Vec);
		auto operator +(const Vector3& Vec);
		auto operator -(const Vector3& Vec);
		float Magnitude(const Vector3& Vec);
		auto Normalize(const Vector3& Vec);
		void operator==(float x) const;
	protected:
		
	};
}