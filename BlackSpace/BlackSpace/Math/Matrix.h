﻿// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 

#pragma once
#include "../Core/BlackSpaceCore.h"
#include "DirectXMath.h"
class Matrix2x2
{
public:
    Matrix2x2(float m0, float m1,
        float m2, float m3);
    void operator=(const Matrix2x2& matrix2);
    void operator-(const Matrix2x2& matrix2);
    void operator+(const Matrix2x2& matrix2);
    void operator/(const Matrix2x2& matrix2);
    void operator*(const Matrix2x2& matrix2);
protected:
    float a, b, c, d{ };
};
class Matrix3x3
{
    Matrix3x3(float m0,float m1, float m2,
					float m3, float m4,float m5);
    void operator=(const Matrix3x3& matrix3);
    void operator-(const Matrix3x3& matrix2);
    void operator+(const Matrix3x3& matrix2);
    void operator/(const Matrix3x3& matrix2);
    void operator*(const Matrix3x3& matrix2);
};

class Matrix4x4 
{
    Matrix4x4(float m0, float m1, float m2, float m3, 
        float m4, float m5, float m6, float m7);

    void operator=(const Matrix4x4& matrix4);
    void operator-(const Matrix4x4& matrix4);
    void operator+(const Matrix4x4& matrix4);
    void operator/(const Matrix4x4& matrix4);
    void operator*(const Matrix4x4& matrix4);
};