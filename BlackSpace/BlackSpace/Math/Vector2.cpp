﻿#include "Vector2.h"


Vector2::Vector2(float X, float Y)
{
    X = A;
    Y = B;
}

auto Vector2::operator=(const Vector2& Vec)
{
    if ((!A == Vec.A || !B) == Vec.B) { return false; }
    else { return true; }
}

auto Vector2::operator*(const Vector2& Vec)
{
    A *= Vec.A;
    B *= Vec.B;
}

auto Vector2::operator/(const Vector2& Vec)
{
    A /= Vec.A;
    B /= Vec.B;
}

auto Vector2::operator-(const Vector2& Vec)
{
    A -= Vec.A;
    B -= Vec.B;
}

auto Vector2::operator+(const Vector2& Vec)
{
    A += Vec.A;
    B += Vec.B;
}
