﻿// *Copyright(C) Raid Studios, All Rights Reserved.

#pragma once
#include "../Core/BlackSpaceCore.h"
class  Vector2
{
public:
    float A, B{};
    Vector2() = default;
    Vector2(const Vector2&) = delete;
    Vector2(float X, float Y);

    auto operator= (const Vector2& Vec);
    auto operator* (const Vector2& Vec);
    auto operator/ (const Vector2& Vec);
    auto operator- (const Vector2& Vec);
    auto operator+ (const Vector2& Vec);
    float magnitude(const Vector2& Vec);
    auto normalize(const Vector2& Vec);
};
