#pragma once
// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 
#include <AK/SoundEngine/Common/AkMemoryMgr.h>
#include <AK/SoundEngine/Common/AkModule.h>
#include "../Core/AllocatorManager.h"
using namespace BlackSpace;
class WwiseBase {

public:
	bool InitSoundEngine();
protected:
	BSResource<AkMemSettings> MemSet;
};