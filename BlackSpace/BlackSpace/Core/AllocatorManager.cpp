
#include "AllocatorManager.h"

template <class A>
BlackSpace::BSResource<A>::BSResource()
{
	
}

template <class A>
BlackSpace::BSResource<A>::~BSResource()
{
	this->DeleteResource();
}

template<class A>
void BlackSpace::BSResource<A>::DeleteResource()
{
	BSResource<nullptr_t> nullres;
	this->SwapResource(nullres);
	this->DeleteResource();
}

template <class A>
A BlackSpace::BSResource<A>::SwapResource(const BSResource& Resource)
{
	//Rework Resource Swapping, we lose memory here.
}

template<class A>
A* BlackSpace::BSResource<A>::operator->() const noexcept
{
	return *_Mybase::_Ptr;
}
