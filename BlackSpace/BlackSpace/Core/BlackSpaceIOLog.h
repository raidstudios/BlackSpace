#pragma once

#include "spdlog/logger.h"
#include "spdlog/common.h"


class  IOLog {
    template <typename A>
	void Info(A& Message);
    template <typename A>
	void Warning(A& Message);
    template <typename A>
	void Error(A& Message);
    template <typename A>
    void DebugOut(A& Message);
};

template<typename A>
inline void IOLog::Info(A& Message)
{
    SPDLOG_INFO(Message);
}

template<typename A>
inline void IOLog::Warning(A& Message)
{
    spdlog::logger::warn(Message);
}
template<typename A>
inline void IOLog::Error(A& Message)
{
    spdlog::logger::error(Message);
}

template<typename A>
inline void IOLog::DebugOut(A& Message)
{
    OutputDebugStringW(static_cast<LPCWSTR>(Message));
}
