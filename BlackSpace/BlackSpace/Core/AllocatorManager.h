#pragma once
/*Copyright(C) Raid Studios, All Rights Reserved.*/
#include "BlackSpaceCore.h"
#include <wrl.h>
#include <string_view>

namespace BlackSpace
{
	template<class A>
	class  BSResource : public std::_Ptr_base<A>
	{
	public:
		using _Mybase = std::_Ptr_base<A>;

		using COM = Microsoft::WRL::ComPtr<A>;
		BSResource();

		virtual ~BSResource();

		A* GetResource();
		A& GetAddress();
		void DeleteResource();
		A SwapResource(const BSResource& Resource);
		A* operator->() const noexcept;
	};

	template <class A>
	A* BSResource<A>::GetResource()
	{
		return *_Mybase;
	}
	template<class A>
	 A& BSResource<A>::GetAddress()
	{
		 return &A;
	}
	 
}
