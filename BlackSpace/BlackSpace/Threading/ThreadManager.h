#pragma once
/*Copyright(C) Raid Studios, All Rights Reserved.
*/
#include "../Core/BlackSpaceCore.h"
#include <functional>
#include <thread>
#include <mutex>
#include <queue>
namespace BlackSpace {
	using ThreadTask = std::function<void()>;
	class  ThreadManager {
	public:
		template<typename A>
		void AddTask(std::shared_ptr<A>& Task);
		bool RemoveTask(ThreadTask& TasktoRemove);

		int ActiveTasks() const { return Tasks; }
		void TaskFinshed() const { -Tasks; }
		template<typename B>
		std::unique_lock<std::mutex> lockmutex(B& mute);
	protected:
		std::atomic<int> Tasks{ 0 };
		mutable std::mutex mute;
		std::queue<ThreadTask> Queue;
	};
}