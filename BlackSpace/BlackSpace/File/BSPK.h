﻿// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 

#pragma once
#include "../Core/BlackSpaceCore.h"
#include "BSFile.h"
#include "IMountPoint.h"

namespace BlackSpace::BSPK {
    struct BSPKHeader
    {
        char ID[5] = { "BSPK" };         //Used to quickly verify file format
        char BSPKVersion = 0;            //The version of the format. This should match with the code that parses it
        uint32_t ContentVersion = 0;       //The version of the content. Used to possibly do patches and updates to files
        char FolderPath[100];           //The path to the original folder
        char PakName[50];               //The name of the pak file
        uint32_t NumEntries = 0;          //The number of directories/files
    };

    struct BSPKFileTableEntry
    {
        char FilePath[255];             //The path to the file (relative to the bspk directory)
        bool Compressed = false;        //True if compressed
        uint32_t UncompressedSize = 0;    //Size before compression
        uint32_t CompressedSize = 0;      //Size after compression
        uint32_t Offset = 0;              //Offset pointing to start of binary data
    };
    class BSPK : public File::IMountPoint
    {
    public:
        virtual bool OnMount() override;
    protected:
        UP<File::BSFile> BSPKFile;
        UP<BSPKHeader> bh;
        UP<BSPKFileTableEntry>bt;
    };
}
