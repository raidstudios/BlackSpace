#include "BSFile.h"
#include <fstream>


BlackSpace::File::BSFile::BSFile(const std::string& filename)
{
}

void BlackSpace::File::BSFile::CreateBaseFile(const char* filename)
{
	//Create A File And Check if the File Handle is Valid.
	
	file = fopen( filename, "w+");
	RAID_ASSERT(file);
	
}

void BlackSpace::File::BSFile::CreateHandledFile(LPCSTR Filename, DWORD Flags, HANDLE FileH)
{
	FHandle = CreateFileA(Filename, Flags, FILE_SHARE_WRITE | FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED, NULL);
	RAID_ASSERT(FHandle);
}

void BlackSpace::File::BSFile::CreateStreamedFile(const char* filename)
{
}

void BlackSpace::File::BSFile::WriteBaseFile(void* Buffer, FILE* file)
{
	
	fprintf(file,( char const*)Buffer);
	RAID_ASSERT(file);
}

void BlackSpace::File::BSFile::WriteHandledFile(LPCVOID Buffer, HANDLE FH)
{
	WriteFile(FH, Buffer, sizeof(Buffer), NULL, NULL);
}

bool BlackSpace::File::BSFile::Exists() const
{
	return false;
}

std::string BlackSpace::File::BSFile::GetDirectoryPath() const
{
	return std::string();
}

std::string BlackSpace::File::BSFile::GetFileName() const
{
	return std::string();
}
