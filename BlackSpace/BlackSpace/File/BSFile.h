#pragma once
#include <boost/asio/io_service.hpp>
#include <boost/asio/windows/object_handle.hpp>
#include <boost/system/error_code.hpp>
#include <boost/asio.hpp>
#include <boost/convert.hpp>
#include "../Core/BlackSpaceCore.h"
#include "../Core/AllocatorManager.h"
using namespace boost::asio;
using namespace boost::system;
namespace BlackSpace::File {
	class BSFile {
	public:
		BSFile(const std::string& filename);
		virtual ~BSFile() = default;
		//Creates A File(C Style) With a Base Interface.
		void CreateBaseFile(const char* filename);
		/// <summary>
		/// Creates a File That is Handled by Windows. 
		/// </summary>
		/// <param name="Filename">The Name Of The File.</param>
		/// <param name="Flags"> Any Windows(DWORD) Flags you want to Pass to the File.(SEE CreateFileA)</param>
		/// <param name="FileH"> Handle to the File.</param>
		void CreateHandledFile(LPCSTR Filename, DWORD Flags, HANDLE FileH);
		/// <summary>
		/// Creates A Streamed File Handled By Boost.Asio.
		/// </summary>
		/// <param name="filename"> name of the streamed file.</param>
		void CreateStreamedFile(const char* filename);
		void WriteBaseFile(void* Buffer, FILE* file);
		void WriteHandledFile(LPCVOID Buffer, HANDLE FH);

		virtual bool OpenRead(bool allowWrite = false) = 0;
		virtual bool OpenWrite(bool append = false, bool allowRead = false) = 0;

		bool virtual Flush() = 0;
		virtual bool Close() = 0;
		virtual bool IsOpen() const = 0;

		bool Exists() const;
		std::string GetDirectoryPath() const;
		std::string GetFileName() const;
	protected:
	public:
		FILE* file;
		HANDLE FHandle{};
	protected:
		boost::asio::io_service iservice;
	};
}