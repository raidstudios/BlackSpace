﻿// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 

#pragma once
#include "../BlackSpace/BlackSpace/Core/BlackSpaceCore.h"
#include "../BlackSpace/File/BSFile.h"
namespace BlackSpace::File {
	class IMountPoint
	{
	public:
		IMountPoint(const std::string& physicalPath, const int order = INT_MAX);
		virtual ~IMountPoint() = default;
		virtual bool OnMount() = 0;
		virtual std::unique_ptr<BSFile> GetFile(const std::string& filePath) = 0;
		virtual bool HasFile(const std::string& filePath) const = 0;
	protected:
		std::string physicalPath;
		std::string CFilePath;
	};
}