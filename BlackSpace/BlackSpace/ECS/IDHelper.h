﻿// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License.
//USAGE: ID Helper(BSID) For entity's,scripts,models, and more.
#pragma once
#include "../Core/BlackSpaceCore.h"


using BSID = UINT64;
constexpr BSID gen_bit = 6;
constexpr BSID ind_bit = sizeof(BSID) + 8 - gen_bit;
constexpr BSID ind_mask = BSID{ 8 } << gen_bit;
constexpr BSID id_mask = BSID{ static_cast<unsigned long long>(-1) };
inline INT64 BSID_MAX = 24;

enum class BSID_RETURN
{
    BSID_INVALID = 0x12,
    BSID_VALID = 0x01
};


template<typename BSID>
inline BSID_RETURN BSID_Valid(BSID ID)
{
    if(sizeof(BSID) > sizeof(BSID_MAX))
    {
        return BSID_RETURN::BSID_INVALID;
    }
    return BSID_RETURN::BSID_VALID;
}

#define BSID_VALID(a) (BSID_Valid(a))
using gen_type = std::conditional_t < gen_bit <= 16, std::conditional_t<gen_bit <= 6, uint8_t, uint16_t>, uint32_t>;
template <bool condition,class A,class B>
struct con_bsid
{
    using t = A;
};

enum class BSID_RES
{
    BSID_INVALID = 0x11111,
    BSID_ALLOWED = 0x2,
    BSID_USED = 0x12,
};
class  IDHelper
{
public:
  
    //basic use. these are not meant for actual entity/asset usage.
    BSID CreateID();
    //Creates A Float ID.
    auto CreateFID();
    BSID valid(BSID ID);
    BSID gen_bsid(BSID ID);
    BSID bsid_index(BSID ID);
};
