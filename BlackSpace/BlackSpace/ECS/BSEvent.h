﻿// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 

#pragma once
#include <functional>

#include "../Core/BlackSpaceCore.h"
	enum class EventT {
		Basic = 0,
		WindowClose,
		WindowResize,
		WindowFocus,
		WindowUnFocused,					//Self Explained, The Types of the BEvents We Can Return.
		KeyPress,
		KeyRelease,
		MousePress, MouseMoved, MouseScroll
	};
	enum Catergory {
		Basic = 0,
		CatergoryApp = 0,
		CatergoryInput = 1,
		CatergoryKey = 2,			//Filters Our Events
		CatergoryMouse = 3,
		CatergoryMBUTTON = 4,
		CatergoryBottleLight = 5,
		CatergoryBottleEntity = 6,
	};

/*Macros For Class USE*/#define BEVENT_CLASS_TYPE(type) static EventT GetType() {return EventT::##type; }\
virtual EventType GetEventType() const override {return GetType(); }\
virtual const char* GetName() const override { return #type; }

#define BEVENT_CLASS_CATERGORY(catergory) virtual int GetCatFlags() const override {return category; }

	class  RaidEvent {
		friend class BottleDispatcher;  //we need the classes to work in harmony,so we added it as a friend class.
	public:
		virtual EventT GetEventT() const = 0;
		virtual const char* GetName() const = 0;
		virtual int GetCatFlags() const = 0;
		virtual std::string ToStr() const { return GetName(); }
		inline bool InCategory(Catergory Cat) {
			return GetCatFlags() & Cat;
		}
	private:
		bool UnhandledEvent = false;
	};
	class RaidDispatcher {
		template<typename A>
		using EventFn = std::function<bool(A&)>;
	public:
		void REventDispatch(RaidEvent& e);
	};
