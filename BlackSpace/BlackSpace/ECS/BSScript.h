﻿// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 

#pragma once
#include "../Core/BlackSpaceCore.h"
#include "BlackSpaceEntity.h"
using namespace BlackSpace::Entity;
namespace BlackSpace::Script {
	enum class  EngineLevels
	{
		EL_NORMAL = 0x12,
		EL_RENDER = 0x1,
		EL_AUDIO = 0x2,
		EL_RESOURCE = 0x3,
		EL_MEMORY = 0x4,
		EL_PHYSICS = 0x5,
	};
	struct ScriptDesc
	{
		//Allowed to be null.
		BSEntity ParentEntity;
		EngineLevels ScriptLevel;
		bool ServerSide;
	};
	class BSScript : public BSEntity
	{
	public:
		BSScript() = default;
		virtual ~BSScript();

		virtual void Begin() {}
		virtual void update() {}

		BSResource<BSScript> CreateScript(BSEntity entity);
	};
}
