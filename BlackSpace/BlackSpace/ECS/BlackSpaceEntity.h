#pragma once
/*Copyright(C) Raid Studios, All Rights Reserved.
 *NOTE: Entity Class needs to be Built off of!
 *USAGE NOTES: to be usable as a entity, a class shall/needs to inherit from "BSEntity" to be . 
*/
#include <deque>

#include "../Core/BlackSpaceCore.h"
#include "../Core/AllocatorManager.h"
#include "IDHelper.h"
#include "../Math/Vector3.h"
#include "../Math/Matrix.h"
#include "IDHelper.h"
using namespace BlackSpace::Math;
namespace BlackSpace::Entity {
     
    struct EntityDesc
    {
	    mutable Vector3 WorldPos;
        Vector3 LocalPos;
        //Rotation should be a Quaternion, but we will use a float array.
        float rotation[4];
        Vector3 WorldScale{ 1.0f,1.0f,1.0f };
        BSID ID;
        
    };
    class BSEntity 
    {
    public:
        BSEntity() = default;
        BSEntity(const EntityDesc& desc);
        BSEntity(const BSID ID);

    	void add_child(const BSEntity& entity);
        void remove_child(const BSEntity& entity);
        bool is_alive(const BSEntity& entity);
        void kill_entity(const BSEntity& entity);


    protected:
        std::vector<BSEntity>c_entities;
        std::deque<BSID> Free_EID;
        IDHelper helper{};
        BSID EntID;
        
    };
}