﻿#include "IDHelper.h"
#define RND_IMPLEMENTATION
#include <cstdint>
#include <libs/rnd.h>

RND_U64 IDHelper::CreateID()
{
    rnd_pcg_t randstore{};
    rnd_pcg_seed(&randstore,0u);
    RND_U64 ID{ 0 };
    for(uint32_t a{}; a < BSID_MAX; a++)
    {
        ID = rnd_pcg_next(&randstore);
    }
    return ID;
}

auto IDHelper::CreateFID()
{
    FLOAT ID = 0.0f;
    rnd_pcg_t rnd{};
    rnd_pcg_seed((rnd_pcg_t*)rand, 0);
    for(FLOAT A = 0; A < 12; A++)
    {
       ID =  rnd_pcg_nextf(&rnd);
    }
    return ID;
}

BSID IDHelper::valid(BSID ID)
{
    return ID != id_mask;
}

BSID IDHelper::gen_bsid(BSID ID)
{
    return ID >> ind_bit & id_mask;
}

BSID IDHelper::bsid_index(BSID ID)
{
    return ID & id_mask;
}
