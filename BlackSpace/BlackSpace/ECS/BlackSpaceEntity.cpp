﻿// Copyright(C) Raid Studios, All Rights Reserved. This Project Uses The LGPL-2.1 License. 

#include "BlackSpaceEntity.h"
using namespace BlackSpace::Entity;


BSEntity ent;
BlackSpace::Entity::BSEntity::BSEntity(const EntityDesc& desc)
{
   
    desc.WorldPos == 0.0f, 0.0f, 0.0f;
    if (desc.ID == 0) { helper.gen_bsid(desc.ID); }
    assert(this->is_alive(ent));
    if(ent.is_alive(ent) == true){}
    //Success!
}

BlackSpace::Entity::BSEntity::BSEntity(const BSID ID)
{
    auto v = BSID_Valid(ID);
    if (static_cast<bool>(v == BSID_RETURN::BSID_INVALID))
    {
        return;
    }
}

void BlackSpace::Entity::BSEntity::add_child(const BSEntity& entity)
{

}

void BlackSpace::Entity::BSEntity::remove_child(const BSEntity& entity)
{
}

bool BlackSpace::Entity::BSEntity::is_alive(const BSEntity& entity)
{
    assert(entity.EntID == 0);
    helper.bsid_index(entity.EntID);
    if (BSID_Valid(entity.EntID) == BSID_RETURN::BSID_INVALID) { return false; }
}

void BlackSpace::Entity::BSEntity::kill_entity(const BSEntity& entity)
{

}



