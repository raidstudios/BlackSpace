#pragma once
#include "../Core/BlackSpaceCore.h"
#include "../Core/AllocatorManager.h"
#include "../ECS/IDHelper.h"
#include "../BlackSpace-Renderer/EngineCore/BlackSpaceRenderCore.h"
using namespace BlackSpace::monitor_info;
using namespace BlackSpace::window_handles;
using WinProc = HRESULT(*)(HWND, UINT, WPARAM, LPARAM);

namespace BlackSpace::Window {
	struct WindowInfo
	{
		WinProc WinP{ nullptr };
		HWND WindowHandle;
		const char* caption{};
		short left;
		short top;
		UINT32 Width;
		UINT32 Height;
		BSID WindowID;
		UINT Style;
		RECT WinRec{ left,top,0 };
	};
	class BSWindow {
	public:
		friend class MI;
		friend class WH;
		int W = 0;
		int H = 0;

	public:
		BSWindow(BSID WinID);
		~BSWindow();
		void CreateBSWindow( UP<WindowInfo> Info);
		void GoFullscreen(bool fullscreen,UP<WindowInfo> wi);
		constexpr bool isFullscreen() { return Fullscreen; }
		HWND WindowHandle();
		void SetWindowCaption(UP<WindowInfo> wi,const char* cap);
		void Resize(UINT32 Width, UINT32 Height);
		void KillWindow(UP<WindowInfo> wp);
		[[nodiscard]] UINT32 Width() const { return W; }
		[[nodiscard]] UINT32 Height() const { return H; }
	private:
		void SendWindowData();
		void StoreWindowInfo(std::vector<WindowInfo>& WinVec,WindowInfo* Inf);
	protected:
		bool WResize{ false };
		BSID WindowID;
		bool Fullscreen;
		HWND WindowH;
		std::vector<WindowInfo> WindowInfoVec{};
	public:
		
	};
}