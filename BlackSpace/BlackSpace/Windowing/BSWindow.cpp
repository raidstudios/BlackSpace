#include "BSWindow.h"
using namespace BlackSpace::Window;
LRESULT CALLBACK IWinProc (HWND H,UINT msg,WPARAM wp, LPARAM lp)
{
	return LRESULT();
}
BlackSpace::Window::BSWindow::BSWindow(BSID WinID)
{
	WinID == BlackSpace::Window::BSWindow::WindowID;
	if (WinID == NULL || nullptr) {
		{
			UP<IDHelper> id = std::make_unique<IDHelper>();
			id->gen_bsid(WinID);
		} //UP(Unique Pointer) Should Delete Right Here.
	}
}

BlackSpace::Window::BSWindow::~BSWindow()
{
}

void BlackSpace::Window::BSWindow::CreateBSWindow( UP<WindowInfo> Info)
{
	//Checks if the PTR is nullptr, and makes it valid.
	if(Info == NULL || nullptr)
	{
		Info = std::make_unique<WindowInfo>();
	}
	Info->Style = CS_HREDRAW | CS_VREDRAW;
	WinProc call = Info ? Info->WinP : nullptr;
	//Set up WIN32 Window Class
	WNDCLASSEX w;
	w.cbSize = sizeof(w);
	w.style = Info->Style;
	w.lpfnWndProc = IWinProc;
	w.cbClsExtra = 0;
	w.cbWndExtra = call ? sizeof(call) : 0;
	w.hInstance = 0;
	w.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	w.hCursor = LoadCursor(NULL, IDC_ARROW);
	w.hbrBackground = CreateSolidBrush(RGB(26, 46, 76));
	w.lpszMenuName = NULL;
	w.lpszClassName = L"BLackSpaceWindow";
	w.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&w);

	//CreateWindow
	
	AdjustWindowRect(&Info->WinRec, Info->Style, FALSE);
	 auto WinCap = Info->caption ? reinterpret_cast<LPCWSTR>(Info->caption) : static_cast<LPCWSTR>(L"BlackSpaceWindow");
	const auto left = (Info && Info->left) ? Info->left : 0;
	const auto top = (Info && Info->top) ? Info->top : 0;
	const auto width = (Info && Info->Height) ? Info->Height : 1024;
	const auto height = (Info && Info->left) ? Info->left : 764;
	Info->WindowHandle = CreateWindowEx(
	0,w.lpszClassName,WinCap,w.style,left,top,width,height,NULL,NULL,NULL,NULL
	);
	if(Info->WindowHandle != NULL || nullptr)
	{
		StoreWindowInfo(WindowInfoVec, Info.get());
		//If The Callback is valid, then set the window ptr to it.
		if(IWinProc)
		{
			SetWindowLongPtr(Info->WindowHandle, 0, reinterpret_cast<LONG_PTR>(IWinProc));
		}
		//Shows and updates the window.
		ShowWindow(Info->WindowHandle, SW_SHOWDEFAULT);
		UpdateWindow(Info->WindowHandle);
	}
}

void BlackSpace::Window::BSWindow::GoFullscreen(bool fullscreen, UP<WindowInfo> wi)
{
	if (fullscreen == true)
	{
		Fullscreen = true;
		GetClientRect(wi->WindowHandle, &wi->WinRec);
		RECT TR;
		GetWindowRect(wi->WindowHandle, &TR);
		wi->Style = 0;
		SetWindowLongPtr(wi->WindowHandle, GWL_STYLE, wi->Style);
		ShowWindow(wi->WindowHandle, SW_MAXIMIZE);
		
	}
	if (fullscreen == false)
	{
		Fullscreen = false;
	}
	SendWindowData();
}

HWND BlackSpace::Window::BSWindow::WindowHandle()
{
	return WindowH;
}

void BlackSpace::Window::BSWindow::SetWindowCaption(UP<WindowInfo> wi, const char* cap)
{
	wi->caption == cap;
	SendWindowData();
}

void BlackSpace::Window::BSWindow::Resize(UINT32 Width, UINT32 Height) {

	WResize = true;
	SendWindowData();
}

void BSWindow::KillWindow(UP<WindowInfo> wp)
{
	RAID_ASSERT(wp);
	DestroyWindow(wp->WindowHandle);
}

void BlackSpace::Window::BSWindow::SendWindowData()
{
	UP<WH> windowh;
	windowh->WindowHandle = WindowH;
	windowh->WidthCallback = W;
	windowh->HeightCallback = H;
	windowh->isWindowed = Fullscreen;
	windowh->IsResizeing = WResize;
}

void BlackSpace::Window::BSWindow::StoreWindowInfo(std::vector<WindowInfo>& WinVec,WindowInfo* Inf)
{
	for(auto l : WinVec)
	{
		for(int a{0}; a< 7; a++)
		{
			WinVec.push_back(l);
		}
	}
}
